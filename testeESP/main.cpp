#define F_CPU 16000000UL
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//==============================================================================

 void sendData(char * dados);
 void AddChar(char * s, char c);
 
 void HCSR04Trig();										//Fun��o para gerar o pulso de trigger para o sensor ultrass�nico


 // --- Mapeamento de Hardware ---
 #define     echo		PORTD2		//bit para leitura do pulso de Echo
 #define	 trigger		PORTD3		//bit para gerar trigger para o sensor ultrass�nico
 #define     ez0			PORTB2


 // --- Mnem�nicos ---
 #define   set_bit(reg,bit)		(reg |= (1<<bit))		//macro para setar um bit de determinado registrador
 #define   clr_bit(reg,bit)		(reg &= ~(1<<bit))		//macro para limpar um bit de determinado registrador

 uint16_t pegaPulsoEcho();						//fun��o para ler pulso de Echo
 uint16_t pegaDist(int i);
 uint16_t pegaPulsoEz0();			
 
 void itoa(int i, char* s);
 char itoc(int i);

 // --- Vari�veis Globais ---
 short counter = 0x00;

 
//==============================================================================
//	Function: getByte
//	Description:
//==============================================================================
char getByte()
{
    while((UCSR0A & 0x80) != 0x80){}; //Espera a recepo de dados.
                                      //RXC0  zerado aps a leitura de UDR0
    return UDR0;//Armazena os dados recebidos em data.
}



//==============================================================================
//	Function: writeString
//	Description:
//==============================================================================
void readString(char *str)
{
    int n=0;
    str[n] = '\0';
    do
    {
        str[n] = getByte();
    }
    while (str[n++] != '\n');
}

//==============================================================================
//	Function: sendByte
//	Description:
//==============================================================================
void sendByte(unsigned char data)
{
	while((UCSR0A & 0x20) != 0x20){}//Verifica se UDR0 pode receber novos dados para Tx.									//
	UDR0 = data;
	while((UCSR0A & 0x40) != 0x40){}//Verifica se os dados j foram "transmitidos".
	UCSR0A = 0x40;//Para zerar TXC0 (bit 6).

}

//==============================================================================
//	Function: writeString
//	Description:
//==============================================================================
void writeString(const char *str)
{
	while (*str != '\0')
	{
		sendByte(*str);
		++str;
	}
}

// --- Desenvolvimento das Fun��es ---

char itoc(int i) {
	switch (i) {
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
	}
}

void itoa(int i, char* s) {
	int m10 = 1;
	int qt = 0;
	int n;
	
	while (((int)(i/m10))>0)
	m10 *= 10;

	m10 /= 10;
	
	while (m10>0) {
		n = (int)(i/m10) - ((int)(i/(m10*10))*10);
		s[qt] = itoc(n);
		m10 /= 10;
		qt++;
	}
	s[qt] = '\0';
}

uint16_t pegaDist(int i)
{
	if (i==1)
	{
		return pegaPulsoEcho()/5.8;
	}
	if (i==2)
	{
		return pegaPulsoEz0()/5.8;
	}
}
	
uint16_t pegaPulsoEcho()								//Fun��o para captura do pulso de echo gerado pelo sensor de ultrassom
{
	uint32_t resultado;								//Vari�veis locais auxiliares
	
	while(!(PIND & (1<<echo)));
	
	//Configura��o do Timer1 para contar o tempo em que o pulso de echo permanecer� em n�vel l�gico alto
	
	TCCR1A = 0x00;										//Desabilita modos de compara��o A e B, Desabilita PWM
	TCCR1B = (1<<CS11);									//Configura Prescaler (F_CPU/8)
	TCNT1  = 0x00;										//Inicia contagem em 0
	
	while((PIND & (1<<echo)) && !(TCNT1 > 60000));
	
	resultado = TCNT1;									//Salva o valor atual de TCNT1 na vari�vel resultado (tempo que o echo ficou em high)
	
	TCCR1B = 0x00;										//Interrompe Timer
	
	
	return (resultado>>1);								//Fun��o retornar� o tempo em microssegundos
	
	
} //end pegaPulsoEcho


void HCSR04Trig()								//gera pulso de Trigger
{
	
	set_bit(PORTD,trigger);
	_delay_us(10);
	clr_bit(PORTD,trigger);
	
} //end HCSR04Trig


void connect(){ 
	char DataReceived[50];
	char cmdbuff[255];
	
	writeString("AT+RST\r\n");
	do{
		readString(DataReceived);
		
	}while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
	writeString(DataReceived);
	
	_delay_ms(5000);
	
	 writeString("AT+CWMODE=1\r\n");
	 do{
		 readString(DataReceived);
		 
	 }while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
	 writeString(DataReceived);
	 
	 _delay_ms(5000);
	 writeString("AT+CWJAP=\"LACCI\",\"lablacci2017\"\r\n");
	 do{
		 readString(DataReceived);
	 }while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
	 writeString(DataReceived);

	 _delay_ms(10000);

	 writeString("AT+CIPMUX=0\r\n");
	 do{
		 readString(DataReceived);
	 }while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
	 writeString(DataReceived);
	 
	 _delay_ms(3000);	
	 
}

// --- Interrup��es ---
ISR(TIMER0_OVF_vect)
{
	counter++;                      //Incrementa counter
	
	if(counter == 0x0F)             //counter igual a 15?
	{                               //Sim...
		
		counter = 0x00;				//zera counter
		
		HCSR04Trig();               //pulso de trigger
		
	} //end if counter
	
} //end ISR Timer0



void init(){
	//Para Fosc = 16M, U2Xn = 0 e um baud rate = 9600, temos que UBRR0 = 103d = 00000000 01100111
	//Para um baud rate = 115200, temos UBRR0 = 8d = 00000000 00001000
	UBRR0H = 0b00000000;//9600 bits/s.
	UBRR0L = 0b00001000;

	UCSR0A = 0b01000000; //TXC0 = 1 (zera esse flag), U2X0 = 0 (velocidade normal),
	//MPCM0 = 0 (desabilita modo multiprocessador).
	UCSR0B = 0b00011000;
	
	UCSR0C = 0b00000110; //Habilita o modo ass�ncrono (UMSEL01/00 = 00), desabilita paridade (UPM01/00 = 00),
	
}


void send(char *message){
	int i;
	while((UCSR0A & 0x20) != 0x20){}//Verifica se UDR0 pode receber novos dados para Tx.
	
	for(i=0; i < (strlen(message) + 1); i++){
		UDR0 = message[i];
		while (!(UCSR0A & (1 << UDRE0)));
	}
	
	UDR0 = '\r';
	while (!(UCSR0A & (1 << UDRE0)));
	UDR0 = '\n';
	while (!(UCSR0A & (1 << UDRE0)));
	
	UCSR0A = 0x40;//Para zerar TXC0 (bit 6).
}


uint16_t pegaPulsoEz0()								//Fun��o para captura do pulso de echo gerado pelo sensor de ultrassom
{
	uint32_t resultado;								//Vari�veis locais auxiliares
	
	while(!(PINB & (1<<ez0)));
		
	TCCR1A = 0x00;										//Desabilita modos de compara��o A e B, Desabilita PWM
	TCCR1B = (1<<CS11);									//Configura Prescaler (F_CPU/8)
	TCNT1  = 0x00;										//Inicia contagem em 0
	
	while((PINB & (1<<ez0)) && !(TCNT1 > 60000));
	
	resultado = TCNT1;									//Salva o valor atual de TCNT1 na vari�vel resultado (tempo que o echo ficou em high)
	
	TCCR1B = 0x00;										//Interrompe Timer
	
	
	return (resultado>>1);								//Fun��o retornar� o tempo em microssegundos
	
}

int main(void)
{
	int pulso,aux;
	char distance[10];
	char data[255];
	init();
	
	set_bit(DDRD,trigger);							    //configura trigger como sa�da
	
	PORTD = 0x00;										//inicia PORTD em low
	
	
	cli();								//Desabilita a interrup��o global
	
	TCNT0 =  0x00;						//Inicia o timer0 em 0
	TCCR0B = 0x04;						//Configura o prescaler para 1:256
	TIMSK0 = 0x01;						//Habilita a interrup��o por estouro do TMR0
	
	sei();								//Habilitar a interrup��o global
	
	
    _delay_ms(5000);
	connect();
 	   while(1)											//Loop infinito
	   {
		pulso = pegaDist(1);
		aux = pegaDist(2);   
		sprintf(data, "distancia0=%d&distancia1=%d\r\n",pulso,aux);
		sendData(data);	
 		   _delay_ms(2000);
 	   } //end while
	
	}
	
	void sendData(char * dados){
		char DataReceived[50];
		char cmdbuff[255];
		char postData[255];
		char strhost[14] = "192.168.1.125";
		char strurl[25] = "/Esp8266/enviardatos.php";
		char type[4] = "UDP";
		char port[3] = "80";
      
		sprintf(postData,"POST %s HTTP/1.1\r\nHost: %s\r\nAccept: */**\r\nContent-Length: %d\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\n%s",strurl,strhost,strlen(dados),dados);   
		 
		 strcpy(cmdbuff, "AT+CIPSTART=\"TCP\",\"192.168.1.125\",80\r\n");

		 writeString(cmdbuff);
		 
		 do{
			 readString(DataReceived);
		 }while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
		 
		 writeString(DataReceived);
		_delay_ms(1000);

		sprintf(cmdbuff, "AT+CIPSEND=%d\r\n",strlen(postData));
	
		writeString(cmdbuff);
		do{
			readString(DataReceived);
		}while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
    
		_delay_ms(1000);

		strcpy(cmdbuff, postData);
		writeString(cmdbuff);
		_delay_ms(500);
		
		 strcpy(cmdbuff, "AT+CIPCLOSE\r\n");

		 writeString(cmdbuff);
		 
		 do{
			 readString(DataReceived);
		 }while(DataReceived[0] != 'O' && DataReceived[1] != 'K');
		 
		 writeString(DataReceived);
		
}
	
void AddChar(char * s, char c) {
		char k;
		k = strlen(s);
		s[k] = c;
		s[k + 1] = 0;
}
	
